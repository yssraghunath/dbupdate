package com.example.demo.utility;

import java.io.Serializable;
import java.util.List;

public interface IOOperation<T> {
	
	void save(T entity);
	T update(T entity);
	void delete(T entity);
	List<T> fatch(T entity);
	List<T> fatch(T entity,String condition);
	T find(T entity,Serializable id);
	T find(T entity,String condition);
	int executeQuery(String query);
	
	
	

}
