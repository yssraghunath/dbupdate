package com.example.demo.service;

import com.example.demo.utility.IOOperation;

public interface IGenericService<T> extends IOOperation<T> {

}
