package com.example.demo.service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduleService {
	
	private static final Logger log = LoggerFactory.getLogger(ScheduleService.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	
	String folders[]={"SelfCare","CENTRUM"};
	
	public int createFolder(String folder ){
		String date=new SimpleDateFormat("dd_MM_yyyy_ss").format(new Date());
		String path="D:/applogs/"+folder+"/"+date+"";
		System.out.println("path "+path);
		File files = new File(path);
        if (!files.exists()) {
            if (files.mkdirs()) {
                System.out.println("Multiple directories are created!");
            } else {
                System.out.println("Failed to create multiple directories!");
            }
        }else{
        	System.out.println("Already Exsit");
        }

	
		return 0;
	}
	
//	@Scheduled(cron = "*/20 * * * * * ")
//	public String runSchedulerOnDay(){
//		log.info("TIME " +dateFormat.format(new Date()));
//		
//		for(String f:folders){
//			createFolder(f);
//		}
//		
//		return "working";
//	}
	
	/*
    @Scheduled(fixedRate = 5000)
    public void reportCurrentTime() {
        log.info("The time is now {}", dateFormat.format(new Date()));
    }
	*/
	
}
