package com.example.demo.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IGenericDao;

@Service
public class ImpGenericSerivce<T> implements IGenericService<T> {

	@Autowired
	IGenericDao<T> dao;
	@Override
	public void save(T entity) {
		// TODO Auto-generated method stub
		dao.save(entity);
	}

	@Override
	public T update(T entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public void delete(T entity) {
		// TODO Auto-generated method stub
		dao.delete(entity);
	}

	@Override
	public List<T> fatch(T entity) {
		// TODO Auto-generated method stub
		return dao.fatch(entity);
	}

	@Override
	public List<T> fatch(T entity, String condition) {
		// TODO Auto-generated method stub
		return dao.fatch(entity, condition);
	}

	@Override
	public T find(T entity, Serializable id) {
		// TODO Auto-generated method stub
		return dao.find(entity, id);
	
	}

	@Override
	public T find(T entity, String condition) {
		// TODO Auto-generated method stub
		return dao.find(entity, condition);
	}

	@Override
	public int executeQuery(String query) {
		// TODO Auto-generated method stub
		return dao.executeQuery(query);
	}

	

}
