package com.example.demo.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DatabaseAction {
	
	String DRIVER="com.mysql.jdbc.Driver";
	String URL="jdbc:mysql://localhost:3306";
	String USER="root";
	String PASSWORD="root";
    String DATABASE_NAME = "test";
    String TABLE_NAME="student";
    String BACKUP_PATH="E:/";
  //  E:\pd
    
    SimpleDateFormat sdf=new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss_a");
	
	public String readCSVFile(String csvFIlePath,String TABLE_NAME ) throws IOException {
		File f=new File(csvFIlePath);
		String query="";
		if(f.isFile()) {
			
			System.out.println("file found");
			FileReader fr = new FileReader(f);
			BufferedReader br=new BufferedReader(fr);
			String line="";
			String dataStr="";
			
			while((line=br.readLine())!=null) {
				
				line="'"+line.replace(",", "','")+"'";
				System.out.println(line);
				dataStr=dataStr+"("+line+"),";
			
				
			}

			query="INSERT INTO "+TABLE_NAME+" VALUES "+dataStr+";";
			System.out.println(query);
			
		}else {
			System.out.println("file not found");
		}

		
		
		return query;
	}

	//backup
		public boolean tableBackUp() {
			Date DATE=new Date();
			String strDate=sdf.format(DATE);
			System.out.println("date "+strDate);
			String source=BACKUP_PATH+"/"+strDate+"_"+TABLE_NAME+".sql";
			System.out.println("source "+source);
			Process p=null;
		try {
				
				Runtime runtime=Runtime.getRuntime();
			// all database backup
			//	p = runtime.exec("mysqldump -uadmin -padmin --add-drop-database -B "+dbName+" -r " + source);
			//ONE TABLE BACKUP
				p = runtime.exec("mysqldump -u"+USER+" -p"+PASSWORD+" --add-drop-database -B "+DATABASE_NAME+" --tables "+TABLE_NAME+" -r " +source);
				
				int res=p.waitFor();
				if(res==0) {
					System.out.println("backup success full");
				}else {
					System.out.println("backup faild");
				}
				
			}catch(Exception e) {
				
				System.out.println(" exp "+e);
			}

			return true;
		}
		
		public boolean tableTrunket(Connection connection) {
					
			try {
									
				  //PreparedStatement ps=connection.prepareStatement("show tables");
					PreparedStatement ps=connection.prepareStatement("TRUNCATE TABLE "+TABLE_NAME);
					boolean i=ps.execute();
				if(i) {
					System.out.println("successFULLY TRUNKET");
					
				}else {
					System.out.println("TRUNKET FAILD faild");
				}
			}catch(Exception  e) {
				System.out.println("exp "+e);
			}
			
			return true;
			
		}
		

	public Connection dbConnection() throws ClassNotFoundException, SQLException {
		Class.forName(DRIVER);
		Connection conn=DriverManager.getConnection(URL+"/"+DATABASE_NAME,USER,PASSWORD);  

		System.out.println("CONNECTION STABLISHED ");
		return conn;
	}
}
