package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.UserEntity;
import com.example.demo.service.IGenericService;

@RestController
@CrossOrigin
public class UserController {
	
	@Autowired
	IGenericService<UserEntity> userService;
	
	@RequestMapping("/list")
	public ResponseEntity<List<UserEntity>> getUserList(){
		
		List<UserEntity> list=userService.fatch(new UserEntity());
		 return new ResponseEntity<List<UserEntity>>(HttpStatus.OK);
	}

	@RequestMapping("/save")
	public ResponseEntity<?> saveUserList(@RequestBody UserEntity entity){
		
		userService.save(entity);
		 return new ResponseEntity<List<UserEntity>>(HttpStatus.OK);
	}

}
