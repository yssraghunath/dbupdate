package com.example.demo.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.bean.QueryBean;
import com.example.demo.service.DatabaseAction;
import com.example.demo.service.IGenericService;

@Controller
public class DatabaseController {
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	private static final Logger log = LoggerFactory.getLogger(StarterController.class);
	private String uploadFolder="H:/xxx";


	@Autowired
	IGenericService<QueryBean> beanService;

	@GetMapping("/table")
	public String getupdate(){
		
		return "table_upload";
	}

	@PostMapping("/table1")
	public String update(Model model,@RequestParam String tableName,@RequestParam MultipartFile file ) throws IOException{
		log.info("   { } "+tableName);
		log.info("file  { } "+file.isEmpty());
		String query=saveFile(file,tableName);
		log.info(" query { } "+query);
		beanService.executeQuery(query);
		return "table_upload";
	}
	
	//save file in folder
	/**
	 * @param multipart
	 * @param tableName
	 * @return
	 * @throws IOException
	 */
	public String saveFile(MultipartFile multipart,String tableName) throws IOException{
		
		String fileName="";
		String name=multipart.getOriginalFilename();
		
		String[] words = name.split("\\s+");
		System.out.println("word "+words);
		for (int i = 0; i < words.length; i++) {
		
					fileName = fileName + words[i].replaceAll(" ", "");
		
			/*mobile.txt*/
			log.info("-----Trim file name----- " + fileName);
			
			
		}
		
		
		File file = new File(fileName);
		File filePath = file.getAbsoluteFile();
		String realPath=filePath.getAbsolutePath();
		String newFilePath=uploadFolder+"/"+fileName;
		Path path=Paths.get(newFilePath);
		log.info("------ Get Real Path Where Doc from Hit.. -------- " + filePath); 
		log.info("------ Get Real Path Where Doc from Hit.. -------- " + realPath); 
		
		byte[] bytes=multipart.getBytes();
		
		Files.write(path,bytes);
		
		
		
		log.info(fileName+"{  }"+path);
		String query=new DatabaseAction().readCSVFile(newFilePath, tableName);
		
		
		
		return query;
	}
	
	
	
	@GetMapping("/query")
	public String queryupdate(){
		
		String query="truncate table school";
		//String query="INSERT INTO school VALUES(2,'adfdf')";
		//String query="INSERT INTO school VALUES(2,'adfdf')";
		//int i=beanService.executeQuery(query);
		//log.info(" value i "+i);
		new DatabaseAction().tableBackUp();
		return "table_upload";
	}


}
