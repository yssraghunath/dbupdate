package com.example.demo.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.entity.UserEntity;

@Controller
@EnableAutoConfiguration
public class StarterController {
private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
private static final Logger log = LoggerFactory.getLogger(StarterController.class);
	






	
	@GetMapping("/hello")
	public String getStart(Model model){
		
		Calendar cal =new GregorianCalendar();
		int month_convert=cal.get(Calendar.MONTH)+1;
		String final_Date=String.valueOf(month_convert)+"/"+String.valueOf(cal.get(Calendar.DAY_OF_MONTH))+"/"+cal.get(Calendar.YEAR);

		model.addAttribute("datesf",final_Date);
		return "index";
	}
	
	@RequestMapping("/login")
	public String goLogin(Model model){
		model.addAttribute("message","Hello login");
		
		return "login";
	}
	
	@RequestMapping(value="/login" ,method=RequestMethod.POST)
	public String userLogin(Model model,HttpServletRequest req, @ModelAttribute("user") UserEntity user){
			log.info("hello "+user.getUsername());
		log.info( req.getAttribute("username")+" kk");
		String user1=(String) req.getAttribute("username");
		String pass=(String) req.getAttribute("password");
		user1=req.getParameter("username");
	log.info(user+" ^^ "+pass);
		model.addAttribute("user", user);
		model.addAttribute("message", "Login");
		return "homepage";
	}
	
	@RequestMapping("/signup")
	public String gosingup(Model model){
		log.info("go to singup page");
		return "signup";
	}
	
	
	@RequestMapping(value="/registration" ,method=RequestMethod.POST)
	public String usersingup(Model model,HttpServletRequest req, @ModelAttribute("user") UserEntity user){
			log.info("hello "+user.getUsername());
		log.info( req.getAttribute("username")+" kk");
		String user1=(String) req.getAttribute("username");
		String pass=(String) req.getAttribute("password");
		user1=req.getParameter("username");
	log.info(user+" ^^ "+pass);
		model.addAttribute("user", user);
		model.addAttribute("message", "Registerd");
		return "success";
	}
	 // w8 i have code of date formate i will do it w8  2 mint ok ? ok
}
