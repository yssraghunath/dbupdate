package com.example.demo.dao;

import com.example.demo.utility.IOOperation;

public interface IGenericDao<T> extends IOOperation<T> {

}
