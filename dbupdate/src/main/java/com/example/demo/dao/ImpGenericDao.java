package com.example.demo.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class ImpGenericDao<T> implements IGenericDao<T> {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	@Override
	public void save(T entity) {
		// TODO Auto-generated method stub
		entityManager.persist(entity);
	}

	@Override
	public T update(T entity) {
		// TODO Auto-generated method stub
		return entityManager.merge(entity);
	}

	@Override
	public void delete(T entity) {
		// TODO Auto-generated method stub
		entityManager.remove(entity);
		
	}


	@Override
	public List<T> fatch(T entity) {
		// TODO Auto-generated method stub
		return entityManager.createQuery("FROM "+entity.getClass().getName()).getResultList();
	}

	@Override
	public List<T> fatch(T entity, String condition) {
		// TODO Auto-generated method stub
	return entityManager.createQuery("FROM "+entity.getClass().getName()+" "+condition).getResultList();
	}

	@Override
	public T find(T entity, Serializable id) {
		// TODO Auto-generated method stub
		return (T) entityManager.find(entity.getClass(), id);
	}

	@Override
	public T find(T entity, String condition) {
		// TODO Auto-generated method stub
		return (T)entityManager.createQuery("From "+entity.getClass().getName()+" "+condition).getResultList().get(0);
	}

	@Override
	public int executeQuery(String query) {
		// TODO Auto-generated method stub
		try{
		//	jdbcTemplate.execute(query);
			int i=jdbcTemplate.update(query);
			return i;
		}catch(DataAccessException e){
			System.out.println(e);
			return 0;
		}
		
	}

	
	
	
	

}
